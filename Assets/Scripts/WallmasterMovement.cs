using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallmasterMovement : MonoBehaviour
{
    public Transform move_point;
    public Transform first_position;
    public Transform second_position;
    public Transform final_position;
    [SerializeField] float movement_speed = 1f;

    GameObject Player;
    ArrowKeyMovement move_controller;
    GameObject pmove_point;
    GameObject hitbox;
    BoxCollider box;
    GameObject main_camera;

    WallmasterGrab grab_controller;
    private void Awake()
    {
        main_camera = GameObject.Find("Main Camera");
        Player = GameObject.Find("Player");
        move_controller = Player.GetComponent<ArrowKeyMovement>();
        pmove_point = Player.transform.GetChild(0).gameObject;
        hitbox = Player.transform.GetChild(1).gameObject;
        box = Player.GetComponent<BoxCollider>();
        grab_controller = GetComponentInChildren<WallmasterGrab>();
    }
    // Start is called before the first frame update
    void Start()
    {
        move_point.parent = null;
        first_position.parent = null;
        second_position.parent = null;
        final_position.parent = null;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartMoving()
    {
        StartCoroutine(Movement());
    }

    IEnumerator Movement()
    {
        yield return StartCoroutine(CoroutineUtilities.MoveObjectOverTime(transform, transform.position, first_position.position,
            Vector3.Distance(transform.position , first_position.position) / movement_speed));
        yield return StartCoroutine(CoroutineUtilities.MoveObjectOverTime(transform, transform.position, second_position.position,
            Vector3.Distance(transform.position, second_position.position) / movement_speed));
        yield return StartCoroutine(CoroutineUtilities.MoveObjectOverTime(transform, transform.position, final_position.position,
            Vector3.Distance(transform.position, final_position.position) / movement_speed));
        if (grab_controller.is_grabbed)
        {
            grab_controller.is_grabbed = false;
            first_position.gameObject.SetActive(true);
            Player.transform.position = new Vector3(40, 5, 0);
            pmove_point.transform.position = Player.transform.position;
            move_controller.moveable = true;
            hitbox.SetActive(true);
            box.enabled = true;
            main_camera.transform.position = main_camera.GetComponent<CameraController>().initial_position;
            
        }
        first_position.gameObject.SetActive(true);
        transform.position = move_point.position;
        yield return null;
    }

   


}

