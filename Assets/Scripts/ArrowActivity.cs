using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowActivity : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // if sword hits camera boundry -> destory sword
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        if(pos.x < 0.03F || 0.97F < pos.x || pos.y < 0.12F || 0.75F < pos.y) {
            Destroy(gameObject);
            GameObject.FindWithTag("Player").GetComponent<ArrowCreate>().bow_exist = false;
        }
    }
    
    
    void OnTriggerEnter(Collider other)
    {
        // if sword hits enemy -> destroy sword
        if (other.gameObject.CompareTag("hitbox") && 
            other.gameObject.transform.parent.gameObject.CompareTag("Enemy") && 
            other.gameObject.GetComponent<HasHealth>().is_invincible == false){
            Destroy(gameObject);
            GameObject.FindWithTag("Player").GetComponent<ArrowCreate>().bow_exist = false;            
        }
    }
}
