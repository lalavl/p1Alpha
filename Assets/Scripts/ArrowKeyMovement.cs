using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowKeyMovement : MonoBehaviour
{
    Rigidbody rb;
    public float movement_speed = 4f;
    public Transform move_point;
    public bool moveable = true;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        move_point.parent = null;
        move_point.position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 cur_input = GetInput();
        if (Vector3.Distance(transform.position, move_point.position) == 0.0f && moveable)
        {
            //Vector3 current_input = GetInput().normalized;
            move_point.position += cur_input.normalized * 0.5f;
            move_point.position = new Vector3(Mathf.Round(move_point.position.x / 0.5f)*0.5f, Mathf.Round(move_point.position.y / 0.5f) * 0.5f, 0);
            UpdateFacingDirection(move_point.position - transform.position);
        }

        //rb.velocity = (move_point.position - transform.position).normalized * movement_speed;
        //if (moveable && cur_input != Vector3.zero){
        //UpdateFacingDirection(cur_input);
        //transform.position = Vector3.MoveTowards(transform.position, move_point.position, movement_speed * Time.deltaTime);
        //}
        transform.position = Vector3.MoveTowards(transform.position, move_point.position, movement_speed * Time.deltaTime);
    }

    void UpdateFacingDirection(Vector3 new_diret)
    {   
        if(new_diret.x > 0){
            GameControl.instance.UpdateFacingDirection("right");
        }else if(new_diret.x < 0){
            GameControl.instance.UpdateFacingDirection("left");
        }else if (new_diret.y > 0){
            GameControl.instance.UpdateFacingDirection("up");
        }else if (new_diret.y < 0){
            GameControl.instance.UpdateFacingDirection("down");
        }
        
    }

    Vector3 GetInput()
    {
        float horizontal_input = Input.GetAxisRaw("Horizontal");
        float vertical_input = Input.GetAxisRaw("Vertical");
        if (Mathf.Abs(horizontal_input) > 0.0f)
        {
            vertical_input = 0.0f;
        }

        return new Vector3(horizontal_input, vertical_input, 0.0f);
    }
    
    public void UpdateMoveable(bool update){
        moveable = update;
    }

    void OnCollisionStay(Collision collision)
    {
        transform.position = new Vector3(Mathf.Round(transform.position.x / 0.5f) * 0.5f, Mathf.Round(transform.position.y / 0.5f) * 0.5f, 0);
        //rb.velocity = Vector3.zero;
        move_point.position = transform.position;
    }
}
