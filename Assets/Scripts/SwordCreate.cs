using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordCreate : MonoBehaviour
{
    public GameObject swordPrefabUp;
    public GameObject swordPrefabDown;
    public GameObject swordPrefabLeft;
    public GameObject swordPrefabRight;
    private Animator animator;
    private GameObject player;
    private GameObject hitbox;
    private int max_health;
    private int cur_health;
    public bool sword_exist = false;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player");
        hitbox = player.transform.Find("Hitbox").gameObject;
        max_health = hitbox.GetComponent<HasHealth>().max_health;
    }

    // Update is called once per frame
    void Update()
    {
        // if key pressed, sword generated
        //print("sword exist is " + sword_exist);
        if (swordActivate() && sword_exist == false)
        {   
            sword_exist = true;
            StartCoroutine(SwingThenDestroy());
        }
    }
    
    // Check if sword is activate at this monment
    bool swordActivate()
    {
        if ((Input.GetKeyDown(KeyCode.X) && GameControl.instance.primaryWeapon == "sword") ||
            (Input.GetKeyDown(KeyCode.Z) && GameControl.instance.altWeapon == "sword"))
        {
            return true;
        }
        return false;
    }

    IEnumerator SwingThenDestroy()
    {
        GameObject sword;
        Vector3 temp;
        cur_health = hitbox.GetComponent<HasHealth>().current_health;
        // TODO: if health is full : swing will move from link to the facing direction
        switch(GameControl.instance.faceDirection){
            case "right":
                // print("animation state: run_right");
                temp = new Vector3(1.0f,0,0);
                sword = (GameObject)Instantiate(swordPrefabRight, transform.position + temp, Quaternion.identity);
                break;
            case "left":
                // print("animation state: run_left");
                temp = new Vector3(1.0f,0,0);
                sword = (GameObject)Instantiate(swordPrefabLeft, transform.position - temp, Quaternion.identity);
                break;
            case "up":
                // print("animation state: run_up");
                temp = new Vector3(0,1.0f,0);
                sword = (GameObject)Instantiate(swordPrefabUp, transform.position + temp, Quaternion.identity);
                break;
            case "down":
                // print("animation state: run_down");
                temp = new Vector3(0,1.0f,0);
                sword = (GameObject)Instantiate(swordPrefabDown, transform.position - temp, Quaternion.identity);
                break;
            default:
                temp = new Vector3(1.0f,0,0);
                sword = (GameObject)Instantiate(swordPrefabRight, transform.position + temp, Quaternion.identity);
                break;
        }
        player.GetComponent<ArrowKeyMovement>().UpdateMoveable(false);
        if (cur_health == max_health){
            StartCoroutine(SwingMove(sword, GameControl.instance.faceDirection));
        }else{
            yield return new WaitForSeconds(0.5f);
            Destroy(sword);
            sword_exist = false;
            player.GetComponent<ArrowKeyMovement>().UpdateMoveable(true);
        }
            
    }

    // Input: "left", "right", "up", "right"
    // Output: change the velocity of the sword the input direction
    IEnumerator SwingMove(GameObject sword, string direction){
        // access the rigid body
        Rigidbody rb = sword.GetComponent<Rigidbody>();
        if (direction == "left")
        {
            rb.velocity = new Vector3(-10, 0, 0);
        }
        else if (direction == "right")
        {
            rb.velocity = new Vector3(10, 0, 0);
        }
        else if (direction == "up")
        {
            rb.velocity = new Vector3(0, 10, 0);
        }
        else
        {
            rb.velocity = new Vector3(0, -10, 0);
        }
        // Update the moveable back to true
        yield return new WaitForSeconds(0.5f);
        player.GetComponent<ArrowKeyMovement>().UpdateMoveable(true);
    }
}
