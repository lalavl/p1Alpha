using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class HealthDisplayer : MonoBehaviour
{
    public HasHealth health;
    TextMeshProUGUI health_text;
    int max_health;
    // Start is called before the first frame update
    void Start()
    {
        health_text = GetComponent<TextMeshProUGUI>();
        max_health = health.get_max_health();
    }

    // Update is called once per frame
    void Update()
    {
        if (health != null && health_text != null)
        {
            health_text.text = "Health: " + health.get_current_health().ToString() + "/" + max_health.ToString();
        }
        if (health.get_current_health() <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
