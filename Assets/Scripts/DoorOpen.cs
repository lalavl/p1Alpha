using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
    public GameObject counterpart;
    public Sprite open_sprite;
    public Sprite counterpart_open_sprite;
    public string door_direc = "down";
    public bool is_locked = false;
    SpriteRenderer own_sprite_renderer;
    SpriteRenderer counterpart_sprite_renderer;
    BoxCollider own_collider;
    BoxCollider counterpart_collider;
    Vector3 target_position;
    // Start is called before the first frame update
    void Start()
    {
        own_sprite_renderer = GetComponent<SpriteRenderer>();
        own_collider = GetComponent<BoxCollider>();
        counterpart_sprite_renderer = counterpart.GetComponent<SpriteRenderer>();
        counterpart_collider = counterpart.GetComponent<BoxCollider>();
        target_position = transform.GetChild(0).transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        
         Inventory inventory = collision.gameObject.GetComponent<Inventory>();
        if (is_locked && inventory != null && (inventory.key_count > 0 || GameControl.instance.god_mode))
        {
            if (!GameControl.instance.god_mode)
            {
                inventory.key_count -= 1;
            }

            is_locked = false;
            own_sprite_renderer.sprite = open_sprite;
            counterpart_sprite_renderer.sprite = counterpart_open_sprite;
        }
        if (!is_locked && inventory != null)
        {
            switch (door_direc)
            {
                case "left":
                    GameObject.Find("Main Camera").GetComponent<CameraController>().roomTransite("right", target_position);
                    break;
                case "right":
                    GameObject.Find("Main Camera").GetComponent<CameraController>().roomTransite("left", target_position);
                    break;
                case "up":
                    GameObject.Find("Main Camera").GetComponent<CameraController>().roomTransite("down", target_position);
                    break;
                case "down":
                    GameObject.Find("Main Camera").GetComponent<CameraController>().roomTransite("up", target_position);
                    break;

            }
        }
       
    }
}
