using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StalfosDefeated : MonoBehaviour
{
    GameObject move_point;
    private void Awake()
    {
        move_point = transform.GetChild(0).gameObject;
    }

    // Start is called before the first frame update
    void Start()
    {
        GetComponentInChildren<HasHealth>().defeated += EnemyDefeated;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void EnemyDefeated()
    {
        Destroy(move_point);
        Destroy(gameObject);
    }
}
