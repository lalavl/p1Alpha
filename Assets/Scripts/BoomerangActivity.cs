using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangActivity : MonoBehaviour
{
    private Vector3 playerPos;
    public float moveSpeed = 4f;
    public GameObject experiment;

    // Start is called before the first frame update
    void Start()
    {
        playerPos = GameControl.instance.GetPlayerPosition();
        experiment = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        // update player new_position
        GameControl.instance.GetPlayerPosition();
        // Rotate the boomerang
        transform.Rotate(new Vector3(0, 0, 720) * Time.deltaTime);

    }
    
    
    void OnTriggerEnter(Collider other)
    {
        // if sword hits enemy -> destroy sword
        if ((other.gameObject.CompareTag("hitbox") && 
            other.gameObject.transform.parent.gameObject.CompareTag("Enemy")) && 
            other.gameObject.GetComponent<HasHealth>().is_invincible == false){
            //print("the weapon touches" + other.gameObject.name);
            GameControl.instance.ChangeCollisionHappen(true);
            // StartCoroutine(BacktoPlayer());
            // UpdateExist();
        }else if (other.gameObject.name == "Tile_WALL"){
            GameControl.instance.ChangeCollisionHappen(true);
            // StartCoroutine(BacktoPlayer());
            // UpdateExist();
        }

        // if(other.gameObject.CompareTag("Player")){
        //     Destroy(gameObject);
        // }
    }

}