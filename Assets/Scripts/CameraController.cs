using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Vector3 initial_position;
    private float camera_height;
    private float camera_width;
    private Camera camera;
    private GameObject player;
    private Transform move_point;
    private void Awake()
    {
        player = GameObject.Find("Player");
        move_point = player.transform.GetChild(0);
    }
    // Start is called before the first frame update
    void Start()
    {
        initial_position = transform.position;
        camera = GetComponent<Camera>();
        camera_height = 1.7f * camera.orthographicSize;
        camera_width = 2f * camera.orthographicSize * camera.aspect;
        
    }

    // move the camera to new room according to new direction
    public void roomTransite(string direction, Vector3 target_position)
    {
        StartCoroutine(WaitForPlayerInputToTransition(direction, target_position));
    }

    IEnumerator WaitForPlayerInputToTransition(string direction, Vector3 target_position)
    {
        Vector3 initial_position = transform.position;
        // Calculate the final position according to direction
        Vector3 final_position = Vector3.zero;
        switch (direction){
            case "right":
                final_position = new Vector3(transform.position.x + camera_width, transform.position.y, -10);
                break;
            case "left":
                final_position = new Vector3(transform.position.x - camera_width, transform.position.y, -10);
                break;
            case "up":
                final_position = new Vector3(transform.position.x, transform.position.y + camera_height, -10);
                break;
            case "down":
                final_position = new Vector3(transform.position.x, transform.position.y - camera_height, -10);
                break;
            default:
                break;
        }

        player.GetComponent<ArrowKeyMovement>().moveable = false;
        /* Transition to new "room" */
        yield return StartCoroutine(
            CoroutineUtilities.MoveObjectOverTime(transform, initial_position, final_position, 2.5f)
        );

        // move player to the new room
        Vector3 temp = Vector3.zero;
        switch (direction){
            case "right":
                temp = new Vector3(5f,0,0);
                break;
            case "left":
                temp = new Vector3(-5f,0,0);
                break;
            case "up":
                temp = new Vector3(0,3.0f,0);
                break;
            case "down":
                temp = new Vector3(0,-3.0f,0);
                break;
            default:
                break;
        }
        //player.transform.position = player.transform.position + temp;
        player.transform.position = target_position;
        player.transform.position = new Vector3(Mathf.Round(player.transform.position.x / 0.5f) * 0.5f, Mathf.Round(player.transform.position.y / 0.5f) * 0.5f, player.transform.position.z);
        player.GetComponent<ArrowKeyMovement>().moveable = true;
        move_point.position = player.transform.position;
    }
}
