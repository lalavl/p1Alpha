using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaldeTrapMove : MonoBehaviour
{
    Transform move_point_h;
    Transform move_point_v;
    Transform original_position;
    Transform target_postion;
    float movement_speed;
    [SerializeField] float forward_speed = 3.0f;
    [SerializeField] float backward_speed = 1.0f;
    //getting child and seperate them from parent, set initial value
    private void Awake()
    {
        move_point_h = transform.GetChild(0);
        move_point_v = transform.GetChild(1);
        original_position = transform.GetChild(2);
    }
    // Start is called before the first frame update
    void Start()
    {
        move_point_h.parent = null;
        move_point_v.parent = null;
        original_position.parent = null;
        target_postion = original_position;
        movement_speed = forward_speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, original_position.position) == 0.0f)
        {
            RaycastHit hit;
            //raycast horizontal
            float length = 2.0f * Vector3.Distance(transform.position, move_point_h.position);
            if (Physics.Raycast(transform.position, transform.TransformDirection(move_point_h.position - transform.position), out hit, length))
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    movement_speed = forward_speed;
                    target_postion = move_point_h;
                }
            }

            //raycast vertically
            length = 2.0f * Vector3.Distance(transform.position, move_point_v.position);
            if (Physics.Raycast(transform.position, transform.TransformDirection(move_point_v.position - transform.position), out hit, length))
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    movement_speed = forward_speed;
                    target_postion = move_point_v;
                }
            }
        }

        //return to original position
        if (Vector3.Distance(transform.position, move_point_h.position) == 0.0f || Vector3.Distance(transform.position, move_point_v.position) == 0.0f)
        {
            movement_speed = backward_speed;
            target_postion = original_position;
        }
        //always move to target position
        transform.position = Vector3.MoveTowards(transform.position, target_postion.position, movement_speed * Time.deltaTime);
    }
}
