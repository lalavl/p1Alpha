using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour
{
    public AudioClip rupee_collection_sound_clip;
    Inventory inventory;
    void Start()
    {
        inventory = GetComponentInParent<Inventory>();
        if (inventory == null)
        {
            Debug.LogWarning("WARNING: gameobject with Collector with no Inventory to store things in");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        GameObject object_collided_with = other.gameObject;
        
        if (inventory != null)
        {
            string tag = object_collided_with.tag;
            switch (tag)
            {
                case "rupee":
                    Destroy(object_collided_with);
                    inventory.AddIventory("Rupee", 1);
                    //play sound effect
                    AudioSource.PlayClipAtPoint(rupee_collection_sound_clip, Camera.main.transform.position);
                    break;
                case "bomb":
                    Destroy(object_collided_with);
                    inventory.AddIventory("Bomb", 1);
                    break;
                case "key":
                    Destroy(object_collided_with);
                    inventory.AddIventory("Key", 1);
                    break;
                default:
                    break;

            }
        }
    }
}
