using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HasHealth : MonoBehaviour
{
    public int max_health = 3;
    public float invincible_time = 1f;
    float invincible_timer;
    public bool is_invincible = false;
    public int current_health;
    Transform move_point;
    GameObject hitbox_parent;
    public System.Action<GameObject> knock_back;
    public System.Action defeated;

    void Awake()
    {
        move_point = transform.parent.gameObject.transform.GetChild(0);
        hitbox_parent = transform.parent.gameObject;
    }
    // Start is called before the first frame update
    void Start()
    {
        current_health = max_health;
        invincible_timer = invincible_time;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (is_invincible)
        {
            invincible_timer -= Time.deltaTime;
        }
        if (invincible_timer <= 0.0f)
        {
            invincible_timer = invincible_time;
            is_invincible = false;
        }
    }

    

    public void AlterHealth(int value, bool cause_knockback, GameObject cause)
    {
        if ((GameControl.instance.godmode_invincible || is_invincible) && value < 0)
        {
            return;
        }
        current_health += value;
        if (value < 0)
        {
            SetInvincible();
            if (knock_back != null && cause_knockback)
            {
                knock_back(cause);
            }
        }
        if (current_health <= 0)
        {
            defeated?.Invoke();
        }
        if (current_health > max_health)
        {
            current_health = max_health;
        }
        
        
    }

    public void SetInvincible()
    {
        if (hitbox_parent.CompareTag("Player"))
        {
            is_invincible = true;
        }
    }
    public int get_current_health()
    {
        return current_health;
    }
    public int get_max_health()
    {
        return max_health;
    }

}
