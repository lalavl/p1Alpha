using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKnockBack : MonoBehaviour
{
    Transform move_point;
    [SerializeField] int back_parameter = 1;

    void Awake()
    {
        move_point = transform.GetChild(0);
    }
    // Start is called before the first frame update
    void Start()
    {
        GetComponentInChildren<HasHealth>().knock_back += PlayerKnocked;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayerKnocked(GameObject attacker)
    {
        float horizontal_differ = transform.position.x - attacker.transform.position.x;
        float vertical_differ = transform.position.y - attacker.transform.position.y;
        if (Mathf.Abs(horizontal_differ) >= Mathf.Abs(vertical_differ))
        {
            vertical_differ = 0.0f;
        }
        if (vertical_differ > Mathf.Abs(horizontal_differ))
        {
            horizontal_differ = 0.0f;
        }
        Vector3 hitting_direction = new Vector3(horizontal_differ, vertical_differ, 0).normalized;
        move_point.position += hitting_direction * back_parameter;
        move_point.position = new Vector3(Mathf.Round(move_point.position.x / 0.5f) * 0.5f, Mathf.Round(move_point.position.y / 0.5f) * 0.5f, 0);
        //string direction = GameControl.instance.faceDirection;
        //switch (direction)
        //{
        //case "down":
        //move_point.position -= Vector3.down.normalized * back_parameter;
        //move_point.position = new Vector3(Mathf.Round(move_point.position.x / 0.5f) * 0.5f, Mathf.Round(move_point.position.y / 0.5f) * 0.5f, 0);
        //break;
        //case "up":
        //move_point.position -= Vector3.up.normalized * back_parameter;
        //move_point.position = new Vector3(Mathf.Round(move_point.position.x / 0.5f) * 0.5f, Mathf.Round(move_point.position.y / 0.5f) * 0.5f, 0);
        //break;
        //case "left":
        //move_point.position -= Vector3.left.normalized * back_parameter;
        //move_point.position = new Vector3(Mathf.Round(move_point.position.x / 0.5f) * 0.5f, Mathf.Round(move_point.position.y / 0.5f) * 0.5f, 0);
        //break;
        //case "right":
        //move_point.position -= Vector3.right.normalized * back_parameter;
        //move_point.position = new Vector3(Mathf.Round(move_point.position.x / 0.5f) * 0.5f, Mathf.Round(move_point.position.y / 0.5f) * 0.5f, 0);
        //break;

        //}
    }
}
