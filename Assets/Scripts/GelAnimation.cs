using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GelAnimation : MonoBehaviour
{
    SpriteRenderer sprite_render;
    [SerializeField] List<Sprite> sprite_list = new List<Sprite>();
    private float move_timer;
    [SerializeField] float move_time = 0.2f;
    private int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        //get reference and set initial value
        sprite_render = GetComponent<SpriteRenderer>();
        move_timer = move_time;
    }

    // Update is called once per frame
    void Update()
    {
        move_timer -= Time.deltaTime;
        if (move_timer <= 0.0f)
        {
            //move index
            index += 1;
            if (index > (sprite_list.Count -1) )
            {
                index = 0;
            }
            //set sprite
            sprite_render.sprite = sprite_list[index];
            //reset timer
            move_timer = move_time;
        }
    }
}
