using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameControl : MonoBehaviour
{
    // initial health: 3 hearts
    public static GameControl instance;
    public string primaryWeapon = "Sword";
    public string altWeapon = "Bow";
    public TextMeshProUGUI primaryUI;
    public TextMeshProUGUI altUI;
    public string faceDirection = "down";
    public bool god_mode = false;
    public bool godmode_invincible = false;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Screen.SetResolution(512, 480,true);
    }

    // Update is called once per frame
    void Update()
    {
        // if space is pressed -> ToggleWeapon
        if (Input.GetKeyDown("space"))
        {
            ToggleWeapon();
        }
        
        UpdateWeaponUI();
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            god_mode = !god_mode;
            godmode_invincible = !godmode_invincible;
        }
    }

    void UpdateWeaponUI()
    {
        primaryUI.text = "Primary: " + primaryWeapon;
        altUI.text = "Alt: " + altWeapon;
    }

    // Toggle between altWeapon and PrimaryWeapon
    void ToggleWeapon()
    {
        string temp = primaryWeapon;
        primaryWeapon = altWeapon;
        altWeapon = temp;
    }

    public void UpdateFacingDirection(string new_diret)
    {
        faceDirection = new_diret;
    }
}
