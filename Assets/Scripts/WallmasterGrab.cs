using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallmasterGrab : MonoBehaviour
{
    GameObject Player;
    ArrowKeyMovement move_controller;
    GameObject move_point;
    GameObject hitbox;
    BoxCollider box;
    public bool is_grabbed = false;

    private void Awake()
    {
        Player = GameObject.Find("Player");
        move_controller = Player.GetComponent<ArrowKeyMovement>();
        move_point = Player.transform.GetChild(0).gameObject;
        hitbox = Player.transform.GetChild(1).gameObject;
        box = Player.GetComponent<BoxCollider>();
    }
    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        if (is_grabbed)
        {
            move_point.transform.position = transform.position;
            Player.transform.position = transform.position;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !GameControl.instance.godmode_invincible)
        {
            is_grabbed = true;
            move_controller.moveable = false;
            hitbox.SetActive(false);
            box.enabled = false;
        }
    }
}
