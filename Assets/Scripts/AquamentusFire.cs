using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AquamentusFire : MonoBehaviour
{
    private float move_timer;
    public float move_time = 1f;
    public GameObject fire;
    public Transform fire_point;
    // Start is called before the first frame update
    void Start()
    {
        move_timer = move_time;
    }

    // Update is called once per frame
    void Update()
    {
        move_timer -= Time.deltaTime;
        if (move_timer <= 0)
        {
            move_timer = move_time;
            GameObject fire1 = Instantiate(fire, fire_point.position, Quaternion.identity);
            fire1.GetComponent<FireMove>().direction = new Vector3(-1, 1, 0).normalized;
            GameObject fire2 = Instantiate(fire, fire_point.position, Quaternion.identity);
            GameObject fire3 = Instantiate(fire, fire_point.position, Quaternion.identity);
            fire3.GetComponent<FireMove>().direction = new Vector3(-1, -1, 0).normalized;
        }
    }
}
