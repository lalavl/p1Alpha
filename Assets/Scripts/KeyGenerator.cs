using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyGenerator : MonoBehaviour
{
    public GameObject key_prefab;
    public Vector3 generate_position = new Vector3(0, 0, 0);
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GenerateKey());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator GenerateKey()
    {

        yield return new WaitUntil(AllEnemyDefeated);

        Instantiate(key_prefab, generate_position, Quaternion.identity);
    }

    bool AllEnemyDefeated()
    {
        int enemy_count = transform.childCount;
        if (enemy_count <= 0)
        {
            return true;
        }
        else
        {
            return false;
        } 

    }
}
