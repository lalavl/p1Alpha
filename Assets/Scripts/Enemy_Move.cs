using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this script give enemy random direction grid based movement
public class Enemy_Move : MonoBehaviour
{
    public Transform move_point;
    public float movement_speed = 4f;
    Rigidbody rb;
    private float move_timer;
    public float move_time = 0.5f;
    public Vector3 face_direction;
    RaycastHit hit;
    //GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        //release the move_point as moving target
        move_point.parent = null;
        move_point.position = transform.position;
        //get reference of other gameobjects
        rb = GetComponent<Rigidbody>();
        move_timer = move_time;
    }

    // Update is called once per frame
    void Update()
    {
        //set timer
        move_timer -= Time.deltaTime;
        //set moving target when timer goes to 0
        if (Vector3.Distance(transform.position, move_point.position) == 0.0f && move_timer <= 0.0f)
        {
            move_timer = move_time;
            face_direction = ComfirmMovement().normalized;
            move_point.position += face_direction;
            move_point.position = new Vector3(Mathf.Round(move_point.position.x), Mathf.Round(move_point.position.y), 0);

        }

        //always move to target
        //Vector3 direction = (move_point.position - transform.position).normalized;
        //transform.position += direction * movement_speed * Time.deltaTime;
        //rb.velocity = direction * movement_speed;
        transform.position = Vector3.MoveTowards(transform.position, move_point.position, movement_speed * Time.deltaTime);
    }

    //find a random position for moving
    Vector3 FindMovement()
    {
        //find the movemen in x or y axis by random
        float horizontal_diff = Random.Range(-2f, 2f);
        float vertical_diff = Random.Range(-2f, 2f);
        if (Mathf.Abs(horizontal_diff) > Mathf.Abs(vertical_diff))
        {
            vertical_diff = 0.0f;
        }
        else
        {
            horizontal_diff = 0.0f;
        }
        return new Vector3(horizontal_diff, vertical_diff, 0);
    }

    //check if the movment will hit a wall
    Vector3 ComfirmMovement()
    {
        bool comfirmed = false;
        Vector3 target_direction =Vector3.left;
        while (!comfirmed)
        {
            target_direction = FindMovement();
            if (Physics.Raycast(transform.position, target_direction, out hit, 1.25f))
            {
                
                if (hit.collider.gameObject.name != "Tile_WALL")
                {
                    comfirmed=  true;
                }


            }
            else
            {
                comfirmed = true;
            }
        }


        return target_direction;
    }

    void OnCollisionStay(Collision collision)
    {
        if (!collision.gameObject.CompareTag(gameObject.tag))
        {
            transform.position = new Vector3(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y), 0);
            move_point.position = transform.position;
        }
        
    }
}
