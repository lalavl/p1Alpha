using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCreate : MonoBehaviour
{
    public GameObject bombPrefab;
    private Animator animator;
    private bool isPrime = false;
    private bool isAlt = false;
    
    // Start is called before the first frame update
    // TODO: how to get the health
    void Start()
    {
        animator = GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        // update current_health
        print("isPrime is " + isPrime);
        if (!isPrime && GameControl.instance.primaryWeapon == "bomb"){
            GetComponent<GameControl>().priWeaponGenerate += ReleaseBomb;
            // print("bomb will be generated soon");
            isPrime = true;
        }
        if (!isAlt && GameControl.instance.altWeapon == "bomb"){
            GetComponent<GameControl>().altWeaponGenerate += ReleaseBomb;
            // print("bomb will be generated soon");
            isAlt = true;
        }
    }

    public void deAttach(){
        if (GameControl.instance.primaryWeapon == "bomb"){
            GetComponent<GameControl>().priWeaponGenerate -= ReleaseBomb;
            isPrime = false;
        }else{
            GetComponent<GameControl>().altWeaponGenerate -= ReleaseBomb;
            isAlt = false;
        }
    }


    void ReleaseBomb(Vector3 position)
    {
        GameObject bomb;
        Vector3 temp;
        // TODO: if health is full : swing will move from link to the facing direction
        switch(GameControl.instance.faceDirection){
            case "right":
                // print("animation state: run_right");
                temp = new Vector3(1.0f,0,0);
                bomb = (GameObject)Instantiate(bombPrefab, position + temp, Quaternion.identity);
                break;
            case "left":
                // print("animation state: run_left");
                temp = new Vector3(1.0f,0,0);
                bomb = (GameObject)Instantiate(bombPrefab, position - temp, Quaternion.identity);
                break;
            case "up":
                // print("animation state: run_up");
                temp = new Vector3(0,1.0f,0);
                bomb = (GameObject)Instantiate(bombPrefab, position + temp, Quaternion.identity);
                break;
            case "down":
                // print("animation state: run_down");
                temp = new Vector3(0,1.0f,0);
                bomb = (GameObject)Instantiate(bombPrefab, position - temp, Quaternion.identity);
                break;
            default:
                temp = new Vector3(1.0f,0,0);
                bomb = (GameObject)Instantiate(bombPrefab, position + temp, Quaternion.identity);
                break;
        }
        GameControl.instance.UpdateMoveable(true);
        StartCoroutine(waitForExplode(bomb));
    }
    
    IEnumerator waitForExplode(GameObject bomb){
        // wait 2 seconds
        yield return new WaitForSeconds(2f);
        // Explode the bomb
        Explode(bomb);
        if (isPrime){
            GameControl.instance.ChangeWeaponExist("prime");
        }else{
            GameControl.instance.ChangeWeaponExist("alt");
        }
    }

    void Explode(GameObject bomb){
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        print(enemies);
        // check distance between bomb and all enemies
        for (int i = 0; i < enemies.Length; i++){
            float distance = Vector3.Distance(bomb.transform.position, enemies[i].transform.position);
            if (distance <= 2){
                enemies[i].GetComponent<HasHealth>().AlterHealth(1);
            }
        }
        Destroy(bomb);
    }

}

