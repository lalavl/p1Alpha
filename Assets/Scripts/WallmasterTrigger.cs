using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallmasterTrigger : MonoBehaviour
{
    WallmasterMovement movement;
    // Start is called before the first frame update
    void Start()
    {
        movement = GetComponentInParent<WallmasterMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            movement.StartMoving();
            gameObject.SetActive(false);
        }
    }
}
