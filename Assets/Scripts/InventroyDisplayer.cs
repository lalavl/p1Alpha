using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventroyDisplayer : MonoBehaviour
{
    public Inventory inventory;
    TextMeshProUGUI ui_text;
    public string item_displayed = "Rupee";
    // Start is called before the first frame update
    void Start()
    {
        ui_text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (inventory != null && ui_text != null)
        {
            ui_text.text = item_displayed + ": " + inventory.GetInventory(item_displayed).ToString();
        }
    }
}
