using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowCreate : MonoBehaviour
{
    public GameObject arrowPrefabUp;
    public GameObject arrowPrefabDown;
    public GameObject arrowPrefabLeft;
    public GameObject arrowPrefabRight;
    private Animator animator;
    private GameObject player;
    private GameObject hitbox;
    public bool bow_exist = false;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        // if key pressed, sword generated
        //print("bow exist is " + bow_exist);
        if (arrowActivate() && bow_exist == false)
        {   
            if (GameControl.instance.god_mode == false)player.GetComponent<Inventory>().AddIventory("Rupee", -1);
            bow_exist = true;
            SwingThenDestroy();
        }
    }
    
    // Check if sword is activate at this monment
    bool arrowActivate()
    {
        if (((Input.GetKeyDown(KeyCode.X) && GameControl.instance.primaryWeapon == "bow") ||
            (Input.GetKeyDown(KeyCode.Z) && GameControl.instance.altWeapon == "bow"))
            && (player.GetComponent<Inventory>().rupee_count > 0 || GameControl.instance.god_mode))
        {
            return true;
        }
        return false;
    }

    void SwingThenDestroy()
    {
        GameObject arrow;
        Vector3 temp;
        // TODO: if health is full : swing will move from link to the facing direction
        switch(GameControl.instance.faceDirection){
            case "right":
                // print("animation state: run_right");
                temp = new Vector3(1.0f,0,0);
                arrow = (GameObject)Instantiate(arrowPrefabRight, transform.position + temp, Quaternion.identity);
                break;
            case "left":
                // print("animation state: run_left");
                temp = new Vector3(1.0f,0,0);
                arrow = (GameObject)Instantiate(arrowPrefabLeft, transform.position - temp, Quaternion.identity);
                break;
            case "up":
                // print("animation state: run_up");
                temp = new Vector3(0,1.0f,0);
                arrow = (GameObject)Instantiate(arrowPrefabUp, transform.position + temp, Quaternion.identity);
                break;
            case "down":
                // print("animation state: run_down");
                temp = new Vector3(0,1.0f,0);
                arrow = (GameObject)Instantiate(arrowPrefabDown, transform.position - temp, Quaternion.identity);
                break;
            default:
                temp = new Vector3(1.0f,0,0);
                arrow = (GameObject)Instantiate(arrowPrefabRight, transform.position + temp, Quaternion.identity);
                break;
        }
        player.GetComponent<ArrowKeyMovement>().UpdateMoveable(false);
        StartCoroutine(SwingMove(arrow, GameControl.instance.faceDirection));
            
    }

    // Input: "left", "right", "up", "right"
    // Output: change the velocity of the sword the input direction
    IEnumerator SwingMove(GameObject arrow, string direction){
        // access the rigid body
        Rigidbody rb = arrow.GetComponent<Rigidbody>();
        if (direction == "left")
        {
            rb.velocity = new Vector3(-10, 0, 0);
        }
        else if (direction == "right")
        {
            rb.velocity = new Vector3(10, 0, 0);
        }
        else if (direction == "up")
        {
            rb.velocity = new Vector3(0, 10, 0);
        }
        else
        {
            rb.velocity = new Vector3(0, -10, 0);
        }
        // Update the moveable back to true
        yield return new WaitForSeconds(0.5f);
        player.GetComponent<ArrowKeyMovement>().UpdateMoveable(true);
    }
}
