using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeHealthOnTouch : MonoBehaviour
{
    [SerializeField] int health_change = -1;
    [SerializeField] bool destroy_on_use = false;
    [SerializeField] bool cause_knockback = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        HasHealth health = other.gameObject.GetComponent<HasHealth>();
        if (destroy_on_use)
        {
            Destroy(gameObject);
        }
        if (health != null)
        {
            health.AlterHealth(health_change, cause_knockback, gameObject);
        }
        
    }
}
