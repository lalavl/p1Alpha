using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public int rupee_count = 0;
    int max_rupee_count = 999;
    public int key_count = 0;
    int max_key_count = 99;
    public int bomb_count = 0;
    int max_bomb_count = 99;
    

    public int GetInventory(string inventory_name)
    {
        switch (inventory_name)
        {
            case "Rupee":
                if (GameControl.instance.god_mode)
                {
                    return max_rupee_count;
                }
                return rupee_count;
                //break;
            case "Key":
                if (GameControl.instance.god_mode)
                {
                    return max_key_count;
                }
                return key_count;
                //break;
            case "Bomb":
                if (GameControl.instance.god_mode)
                {
                    return max_bomb_count;
                }
                return bomb_count;
                //break;
            default:
                return -99;
                //break;


        }

    }

    public void AddIventory(string inventory_name, int num_inventory)
    {
        switch (inventory_name)
        {
            case "Rupee":
                rupee_count += num_inventory;
                break;
            case "Key":
                key_count += num_inventory;
                break;
            case "Bomb":
                bomb_count += num_inventory;
                break;
            default:
                Debug.LogWarning("Adding unknown inventory, please try again");
                break;


        }
    }

    
}
