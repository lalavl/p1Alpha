using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireMove : MonoBehaviour
{
    public Vector3 direction = Vector3.left;
    public float speed = 3f;
    private float destroy_timer;
    public float destroy_time = 5f;
    // Start is called before the first frame update
    void Start()
    {
        destroy_timer = destroy_time;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += direction * speed * Time.deltaTime;
        destroy_timer -= Time.deltaTime;
        if (destroy_timer <= 0)
        {
            Destroy(gameObject);
        }
    }
}
