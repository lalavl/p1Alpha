using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeeseMove : MonoBehaviour
{
    bool moving = false;
    private float move_timer;
    public float move_time = 1f;
    public Transform move_point;
    public float movement_speed = 1f;
    KeeseAnimation anim;

    private void Awake()
    {
        move_point = transform.GetChild(0);
        anim = GetComponent<KeeseAnimation>();
    }

    // Start is called before the first frame update
    void Start()
    {
        move_timer = move_time;
        move_point.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (!moving)
        { 
        move_timer -= Time.deltaTime;
        }
        if (move_timer <= 0.0f)
        {
            StartCoroutine(move());
            move_timer = move_time;
        }
    }

    IEnumerator move()
    {
        float hori = Random.Range(transform.position.x - 5, transform.position.x + 5);
        float verti = Random.Range(transform.position.y - 5, transform.position.y + 5);
        move_point.transform.position = new Vector3(Mathf.Round(hori), Mathf.Round(verti), transform.position.z);
        moving = true;
        anim.move_time = 0.05f;
        yield return StartCoroutine(CoroutineUtilities.MoveObjectOverTime(transform, transform.position,
            Vector3.Lerp(transform.position, move_point.transform.position, 0.5f),
            Vector3.Distance(transform.position, Vector3.Lerp(transform.position, move_point.transform.position, 0.5f)) / movement_speed));
        anim.move_time = 0.125f;
        yield return StartCoroutine(CoroutineUtilities.MoveObjectOverTime(transform, transform.position,
            move_point.transform.position,
            Vector3.Distance(transform.position, move_point.transform.position) / movement_speed));
        moving = false;
        anim.move_time = 0.25f;
        yield return null;
    }
}
