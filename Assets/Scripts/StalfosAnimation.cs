using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StalfosAnimation : MonoBehaviour
{
    SpriteRenderer sprite_render;
    private float move_timer;
    public float move_time = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        sprite_render = GetComponent<SpriteRenderer>();
        move_timer = move_time;
    }

    // Update is called once per frame
    void Update()
    {
        move_timer -= Time.deltaTime;
        if (move_timer <= 0.0f)
        {
            sprite_render.flipX = !sprite_render.flipX;
            move_timer = move_time;
        }
    }
}
