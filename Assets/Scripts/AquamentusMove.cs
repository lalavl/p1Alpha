using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AquamentusMove : MonoBehaviour
{
    public float movement_speed = 1f;
    private float move_timer;
    public float move_time = 0.5f;
    bool forward = false;
    public Transform move_point;
    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        move_point.parent = null;
        move_timer = move_time;
    }

    // Update is called once per frame
    void Update()
    {
        //set timer
        move_timer -= Time.deltaTime;
        if (move_timer <= 0 && Vector3.Distance(move_point.position, transform.position) == 0)
        {
            forward = !forward;
            move_timer = move_time;
            if (forward)
            {
                move_point.position = transform.position + Vector3.left * movement_speed;
            }
            else
            {
                move_point.position = transform.position - Vector3.left * movement_speed;
            }
        }

        transform.position = Vector3.MoveTowards(transform.position, move_point.position, movement_speed * Time.deltaTime);
    }
}
