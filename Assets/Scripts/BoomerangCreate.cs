using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangCreate : MonoBehaviour
{
    public GameObject boomerangPrefab;
    private Animator animator;
    private GameObject hitbox;
    public bool boomerang_exist = false;
    private bool isPrime = false;
    private bool isAlt = false;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPrime && GameControl.instance.primaryWeapon == "boomerang"){
            GetComponent<GameControl>().priWeaponGenerate += SwingThenDestroy;
            isPrime = true;
        }
        if (!isAlt && GameControl.instance.altWeapon == "boomerang"){
            GetComponent<GameControl>().altWeaponGenerate += SwingThenDestroy;
            isAlt = true;
        }
    }

    public void deAttach(){
        if (GameControl.instance.primaryWeapon == "boomerang"){
            GetComponent<GameControl>().priWeaponGenerate -= SwingThenDestroy;
            isPrime = false;
        }else{
            GetComponent<GameControl>().altWeaponGenerate -= SwingThenDestroy;
            isAlt = false;
        }
    }
    

    void SwingThenDestroy(Vector3 position)
    {
        GameObject boomerang;
        Vector3 temp;
        // TODO: if health is full : swing will move from link to the facing direction
        switch(GameControl.instance.faceDirection){
            case "right":
                // print("animation state: run_right");
                temp = new Vector3(1.0f,0,0);
                boomerang = (GameObject)Instantiate(boomerangPrefab, position + temp, Quaternion.identity);
                break;
            case "left":
                // print("animation state: run_left");
                temp = new Vector3(1.0f,0,0);
                boomerang = (GameObject)Instantiate(boomerangPrefab, position - temp, Quaternion.identity);
                break;
            case "up":
                // print("animation state: run_up");
                temp = new Vector3(0,1.0f,0);
                boomerang = (GameObject)Instantiate(boomerangPrefab, position + temp, Quaternion.identity);
                break;
            case "down":
                // print("animation state: run_down");
                temp = new Vector3(0,1.0f,0);
                boomerang = (GameObject)Instantiate(boomerangPrefab, position - temp, Quaternion.identity);
                break;
            default:
                temp = new Vector3(1.0f,0,0);
                boomerang = (GameObject)Instantiate(boomerangPrefab, position + temp, Quaternion.identity);
                break;
        }
        StartCoroutine(SwingMove(boomerang, GameControl.instance.faceDirection)); 
            
    }

    // Input: "left", "right", "up", "right"
    // Output: change the velocity of the sword the input direction
    IEnumerator SwingMove(GameObject boomerang, string direction){
        // access the rigid body
        Rigidbody rb = boomerang.GetComponent<Rigidbody>();
        Vector3 new_pos = Vector3.zero;
        if (direction == "left")
        {
            new_pos = GameControl.instance.GetPlayerPosition() + new Vector3(-6, 0, 0);
        }
        else if (direction == "right")
        {
            new_pos = GameControl.instance.GetPlayerPosition() + new Vector3(6, 0, 0);
        }
        else if (direction == "up")
        {
            new_pos = GameControl.instance.GetPlayerPosition() + new Vector3(0, 6, 0);
        }
        else
        {
            new_pos = GameControl.instance.GetPlayerPosition() + new Vector3(0, -6, 0);
        }
        // Update the moveable back to true
        StartCoroutine(BacktoPlayer(boomerang, new_pos));
        yield return new WaitForSeconds(0.5f);
        GameControl.instance.UpdateMoveable(true);
    }

    IEnumerator BacktoPlayer(GameObject boomerang, Vector3 target_pos){
        float initial_time = Time.time;
        float duration_sec = 1.0f;
        float progress = (Time.time - initial_time) / duration_sec;
        // move to the target postion
        //print("Current Collision status is " + GameControl.instance.getCollisionHappen());
        while (progress <= 1.0f && !GameControl.instance.getCollisionHappen()){
            progress = (Time.time - initial_time) / duration_sec;
            boomerang.transform.position = Vector3.Lerp(GameControl.instance.GetPlayerPosition(), target_pos, progress);
            yield return null;
        }
        // move back to player's position
        target_pos = boomerang.transform.position;

        initial_time = Time.time;
        progress = (Time.time - initial_time) / duration_sec;
        
        while(progress <= 1.0f){
            progress = (Time.time - initial_time) / duration_sec;
            boomerang.transform.position = Vector3.Lerp(target_pos, GameControl.instance.GetPlayerPosition(), progress);
            yield return null;
        }
        boomerang.transform.position = GameControl.instance.GetPlayerPosition();
        GameControl.instance.ChangeCollisionHappen(false);
        Destroy(boomerang);
        if (isPrime){
            GameControl.instance.ChangeWeaponExist("prime");
        }else{
            GameControl.instance.ChangeWeaponExist("alt");
        }
    }

}
